public class Main {

    public static void main(String[] args) {

        Student s1 = new Student("Max");
        //s1.setName("Max");
        s1.sayHello();

        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.party();
        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.study();
        s1.study();

        Student s2 = new Student("List");
        s2.sayHello();

        s2.study();
        s2.study();
        s2.study();
        s2.study();
        s2.study();
        s2.study();
    }

}
