public class Student {

    private String name;
    private int knowledge = 10;
    private int motivation = 10;

    public Student(String studentName) {
        name = studentName;
    }


    public void sayHello() {
        System.out.println("Hallo, ich bin " + name + ". Wissen=" + knowledge + " Motivation=" + motivation);
    }

    public void study() {

        if (motivation <= 0) {
            System.out.println("Heute nicht");
        } else {
            System.out.println("lerne...");
            knowledge = knowledge + 1;
            motivation = motivation - 1;
        }
    }

    public void party() {
        motivation = motivation + 5;
    }

    public int getMotivation() {
        return motivation;
    }


}
